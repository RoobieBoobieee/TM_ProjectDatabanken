<?php
include('session.php');
?>
<!DOCTYPE html>
<html>
<head>
<title>Profile Marketer
</title>
<link rel="stylesheet" href="jquery-ui/jquery-ui.js">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="profile">
    
    
<b id="welcome">Welcome : <i><?php echo $login_session; ?></i></b>

<b id="logout"><a href="logout.php">Log Out</a></b></br>
    <div id="marketer">
          <div id="accordion">
<?php
/*
$min_epoch = strtotime('2014-12-23');
$max_epoch = strtotime('2015-05-29');


$query = "SELECT `Vin_Number` FROM $db.`Sale`;";
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                $data2 = $data['Vin_Number'];
                    $rand_epoch = mt_rand($min_epoch, $max_epoch);
                    $date = date("Y-m-d", $rand_epoch);
                    $query = "UPDATE  $db.`Sale` SET  `Sale_Date` =  '$date' WHERE  `Vin_Number` =  '$data2';";
                
                    mysql_query($query)or die(mysql_error());
            }*/
/*

$query = "SELECT `Vin_Vin_Number`, `Address_Id` FROM $db.`CurrentStock` Limit 100;";
  
$min_epoch = strtotime('2014-12-23');
$max_epoch = strtotime('2015-05-29');  
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                $rand_epoch = mt_rand($min_epoch, $max_epoch);
                $date = date("Y-m-d", $rand_epoch);
                $vin = $data['Vin_Vin_Number'];
                $showroom = $data['Address_Id'];
                $customer = mt_rand(1,100);
                $price = mt_rand(10000,100000);
                $query2 = "INSERT INTO $db.`Sale` (`Customer_Id`, `Vin_Number`, `Sale_Date`, `Sale_Price`, `Sale_Extra`, `Showroom_Id`) VALUES ('$customer', '$vin', '$date', '$price', NULL, '$showroom');";
                
                mysql_query($query2)or die(mysql_error());
            }

*/




echo "<h3># Totale verkoop per model </h3><div>";
$query = "SELECT `Models_Name` FROM $db.`Models`;";
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                $tmp = $data['Models_Name'];
                $query2 = "SELECT SUM(`Sale_Price`) AS TotalPrice FROM $db.`Sales_Models` WHERE `Models_Name` = '$tmp';";
                $result2 = mysql_query($query2)or die(mysql_error());
                $data2 = mysql_fetch_assoc($result2);
                if ($data2['TotalPrice']) {
                echo $tmp . " - €" . $data2['TotalPrice']. "</br>";
                }
            }
echo "</div><h3># Totale verkoop per merk </h3><div>";
$query = "SELECT DISTINCT `Brand_Name` FROM $db.`Models`;";
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                $tmp = $data['Brand_Name'];
                $totaalprijs = 0;
                
                $query = "SELECT `Models_Name` FROM $db.`Models` WHERE `Brand_Name` = '$tmp';";
                $result3 = mysql_query($query)or die(mysql_error());
                while($data3 = mysql_fetch_assoc($result3)) {
                    $tmp2 = $data3['Models_Name'];
                    $query2 = "SELECT SUM(`Sale_Price`) AS TotalPrice FROM $db.`Sales_Models` WHERE `Models_Name` = '$tmp2'  ;";
                    $result2 = mysql_query($query2)or die(mysql_error());
                    $data2 = mysql_fetch_assoc($result2);
                    if ($data2['TotalPrice']) {
                    $totaalprijs += $data2['TotalPrice'];
                    
                    }
                    
                }
                If ($totaalprijs){
                Echo $tmp . " - €" . $totaalprijs . "</br>";
                }
            }
echo "</div><h3># Totale verkoop per showroom </h3><div>";
$query = "SELECT DISTINCT `Address_Id` FROM $db.`Showrooms`;";
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                $tmp = $data['Address_Id'];
                $query2 = "SELECT SUM(`Sale_Price`) AS TotalPrice FROM $db.`Sales_Models` WHERE `Showroom_Id` = '$tmp';";
                $result2 = mysql_query($query2)or die(mysql_error());
                $data3 = mysql_fetch_assoc($result2);
                if ($data3['TotalPrice']) {
                    $query2 = "SELECT `Address`, `City_Postal`, `City_Name` FROM $db.`Address` WHERE `Address_Id` = '$tmp'";
                    $result2 = mysql_query($query2)or die(mysql_error());
                  $data2 = mysql_fetch_assoc($result2);
                    
                            $address = $data2['Address'];
                           $postcode = $data2['City_Postal'];
                           $stad = $data2['City_Name'];
                           echo "$address, $postcode $stad";
                
                echo  " - €" . $data3['TotalPrice']. "</br>";
                }
            }

echo "</div><h3># Verkooptrends </h3><div>";
echo "<table width=100%><tr style='font-weight:bold'><td>Merk</td><td>1 Week</td><td>1 Maand</td><td>1 Jaar</td><td>3 Jaar </td></tr>";

$query = "SELECT DISTINCT `Brand_Name` FROM $db.`Models`;";
$result = mysql_query($query)or die(mysql_error());
$vandaag = date('Y-m-d');

            while($data = mysql_fetch_assoc($result))
            {
                $brand = $data['Brand_Name'];
                echo "<tr><td style='font-weight:bold'>$brand</td>";
                 $query2 = "SELECT `Models_Name`, COUNT(*) FROM $db.`Sales_Models` WHERE `Brand_Name` = '$brand' AND '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 7 DAY)GROUP BY `Models_Name` ORDER BY 2 DESC LIMIT 1;"; 
                $result2 = mysql_query($query2)or die(mysql_error());
                $data2 = mysql_fetch_assoc($result2);
                $model = $data2['Models_Name'];
                echo "<td>$model</td>";
                
                
                 $query2 = "SELECT `Models_Name`, COUNT(*) FROM $db.`Sales_Models` WHERE `Brand_Name` = '$brand' AND '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 31 DAY)GROUP BY `Models_Name` ORDER BY 2 DESC LIMIT 1;"; 
                $result2 = mysql_query($query2)or die(mysql_error());
                $data2 = mysql_fetch_assoc($result2);
                $model = $data2['Models_Name'];
                echo "<td>$model</td>";
                
                
                 $query2 = "SELECT `Models_Name`, COUNT(*) FROM $db.`Sales_Models` WHERE `Brand_Name` = '$brand' AND '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 365 DAY)GROUP BY `Models_Name` ORDER BY 2 DESC LIMIT 1;"; 
                $result2 = mysql_query($query2)or die(mysql_error());
                $data2 = mysql_fetch_assoc($result2);
                $model = $data2['Models_Name'];
                echo "<td>$model</td>";
                
                
                $query2 = "SELECT `Models_Name`, COUNT(*) FROM $db.`Sales_Models` WHERE `Brand_Name` = '$brand' AND '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 1095 DAY)GROUP BY `Models_Name` ORDER BY 2 DESC LIMIT 1;"; 
                $result2 = mysql_query($query2)or die(mysql_error());
                $data2 = mysql_fetch_assoc($result2);
                $model = $data2['Models_Name'];
                echo "<td>$model</td></tr>";
                
                
            
                
                
            }
echo "</table>";

echo "</div><h3># Top 3 Merken - Meest Opgebracht - v/h Afgelopen Jaar </h3><div>";

$query = "SELECT `Brand_Name`, SUM(`Sale_Price`) AS TotaalOpgebracht FROM $db.`Sales_Models` WHERE '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 365 DAY) GROUP BY `Brand_Name` ORDER BY 2 DESC LIMIT 3;"; 
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                echo $data['Brand_Name'];
                echo "<br />";
                
            }

echo "</div><h3># Top 3 Merken - Meeste Voertuigen - v/h Afgelopen Jaar </h3><div>";

$query = "SELECT `Brand_Name`, COUNT(*) FROM $db.`Sales_Models` WHERE '$vandaag' <=  DATE_ADD(`Sale_Date`, INTERVAL 365 DAY) GROUP BY `Brand_Name` ORDER BY 2 DESC LIMIT 3;"; 
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                echo $data['Brand_Name'];
                echo "<br />";
                
            }


echo "</div><h3># Maand Met Meest Verkochte Cabrios </h3><div>";


           

$query = "SELECT MONTHNAME(`Sale_Date`) AS maand, COUNT(`Sale_Date`) AS totaal FROM $db.`FuckingCabrios` GROUP BY maand ORDER BY totaal DESC LIMIT 1;"; 
$result = mysql_query($query)or die(mysql_error());
while($data = mysql_fetch_assoc($result))
{
                
                echo $data['maand'];
    echo ". Voor totaal: ";
                echo $data['totaal'];
                echo " cabrios.<br />";
}




echo "</div><h3># Dealers Die Voertuigen Het Langst In Stok Houden </h3><div>";
for ($i=0; $i<3; $i += 1){
    
 $query = "SELECT `Address`,  `City_Name` FROM $db.`Address` WHERE `Address_Id` = (SELECT  `Showroom_Id` FROM  `Dates` ORDER BY  `verschil` ASC LIMIT $i, 1)";
$result = mysql_query($query)or die(mysql_error());
            while($data = mysql_fetch_assoc($result))
            {
                echo $data['Address'];
                echo " - ";
                echo $data['City_Name'];
                echo "<br />";
                
            }

}




?>
              
        </div>       
        </div>  
    </div>
</div>
<script src="jquery-ui/external/jquery/jquery.js"></script>
<script src="jquery-ui/jquery-ui.js"></script>
<script>

$( "#accordion" ).accordion();
$( ".selector" ).accordion({
  heightStyle: "content"
});
    </script>
</body>
</html>